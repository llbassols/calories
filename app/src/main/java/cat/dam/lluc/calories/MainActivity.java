package cat.dam.lluc.calories;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;

import java.util.ArrayList;

public class MainActivity extends Activity {
    private Button btn_desa, btn_mostra, btn_elimina, btn_modifica, btn_info, btn_suma,btn_restablir;
    private EditText et_nom, et_kal , et_sol,et_fam, et_nomsuma;
    private TextView tv_noms,tv_total;
    private ArrayList<String> arrayList;
    private DatabaseHelper databaseHelper;
    public int kal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        databaseHelper = new DatabaseHelper(this);
        tv_noms = (TextView) findViewById(R.id.tv_noms);
        tv_total = (TextView) findViewById(R.id.tv_total);
        btn_desa = (Button) findViewById(R.id.btn_desa);
        btn_mostra = (Button) findViewById(R.id.btn_mostra);
        btn_elimina = (Button) findViewById(R.id.btn_elimina);
        btn_modifica = (Button) findViewById(R.id.btn_modifica);
        btn_info= (Button) findViewById(R.id.btn_info);
        btn_suma = (Button) findViewById(R.id.btn_suma);
        btn_restablir = (Button) findViewById(R.id.btn_RESTABLIR);
        et_nom = (EditText) findViewById(R.id.et_nom);
        et_kal = (EditText) findViewById(R.id.et_kal);
        et_fam = (EditText) findViewById(R.id.et_fam);
        et_sol = (EditText) findViewById(R.id.et_sol);
        et_nomsuma = (EditText) findViewById(R.id.et_nomsuma);
        kal = 0;


        btn_desa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseHelper.afegeix_detallaliment(et_nom.getText().toString(),et_kal.getText().toString(),et_fam.getText().toString(),et_sol.getText().toString());
                et_nom.setText("");
                et_kal.setText("");
                et_fam.setText("");
                et_sol.setText("");
                Toast.makeText(MainActivity.this, "S'ha desat correctament!",
                        Toast.LENGTH_SHORT).show();
            }
        });
        btn_mostra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrayList = databaseHelper.obte_arrayList_Aliment();
                for (int i = 0; i < arrayList.size(); i++){
                    tv_noms.setText(tv_noms.getText().toString()+" -"+arrayList.get(i));
                }
            }
        });

        btn_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                Bundle b = new Bundle();
                intent.putExtras(b);
                startActivity(intent);

            }
        });
        btn_elimina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseHelper.elimina(et_nom.getText().toString());
                et_nom.setText("");
                et_kal.setText("");
                et_fam.setText("");
                et_sol.setText("");
                Toast.makeText(MainActivity.this, "S'ha eliminat",
                        Toast.LENGTH_SHORT).show();
            }
        });

        btn_modifica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseHelper.modifica(et_nom.getText().toString(),et_kal.getText().toString());
                et_nom.setText("");
                et_kal.setText("");
                et_fam.setText("");
                et_sol.setText("");
                Toast.makeText(MainActivity.this, "S'ha modificat",
                        Toast.LENGTH_SHORT).show();
            }
        });
        /*
        btn_suma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String valor = "";
                valor = databaseHelper.obte_kal(et_nomsuma.getText().toString();
                et_nomsuma.setText("");
                Toast.makeText(MainActivity.this, "S'ha sumat",
                        Toast.LENGTH_SHORT).show();
                kal = kal + Integer.parseInt(valor);
                tv_total.setText(""+kal);
            }
        });
        */
        btn_restablir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kal = 0;
                tv_total.setText(""+kal);
            }
        });
    }
}
