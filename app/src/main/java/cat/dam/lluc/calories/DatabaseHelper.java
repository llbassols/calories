package cat.dam.lluc.calories;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static String NOM_BD = "bd_aliments";
    private static final int VERSIO_BD = 1;
    private static final String TAULA_ALIMENTS = "aliments";
    private static final String CLAU_ID = "id";
    private static final String CLAU_NOM = "nom";
    private static final String CLAU_KAL = "kalories";
    private static final String CLAU_FAM = "familia";
    private static final String CLAU_SOL = "solid";
    // Utilitza un String parametritzat per a la instrucció que crea de la taula alumnes

    private static final String CREA_TAULA_ALIMENTS = "CREATE TABLE "
            + TAULA_ALIMENTS + "(" + CLAU_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT," + CLAU_NOM + " TEXT ," + CLAU_KAL + "  TEXT," + CLAU_FAM + " TEXT," + CLAU_SOL + "  TEXT );";
    public DatabaseHelper(Context context) {
        super(context, NOM_BD, null, VERSIO_BD);
        Log.d("table", CREA_TAULA_ALIMENTS);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        //quan es crea l'objecte a partir de la classe crearà la taula si no està creada
        db.execSQL(CREA_TAULA_ALIMENTS);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int versio_antiga, int versio_nova) {
        db.execSQL("DROP TABLE IF EXISTS '" + TAULA_ALIMENTS + "'");
        onCreate(db);
    }
    public long afegeix_detallaliment(String nom,String kal, String sol, String fam) {
        SQLiteDatabase db = this.getWritableDatabase();
        // Crea valors de contingut
        ContentValues valors = new ContentValues();
        valors.put(CLAU_NOM, nom );
        valors.put(CLAU_KAL, kal );
        valors.put(CLAU_SOL, sol );
        valors.put(CLAU_FAM, fam );

        // inserta una fila a la taula alumnes
        long insert = db.insert(TAULA_ALIMENTS, null, valors);
        return insert;
    }

    public boolean elimina(String name)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TAULA_ALIMENTS, CLAU_NOM + "=?", new String[]{name}) > 0;
    }

    public void modifica(String name,String kalnova)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues valors = new ContentValues();
        valors.put(CLAU_KAL,kalnova);
        db.update(TAULA_ALIMENTS, valors,CLAU_KAL + "=?",new String[]{name});
    }

    public ArrayList<String> obte_arrayList_Aliment() {
        ArrayList<String> arrayList_Aliment = new ArrayList<String>();
        String nom="";
        String selectQuery = "SELECT * FROM " + TAULA_ALIMENTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        // Recorre totes les files i les afegeix a la llista
        if (c.moveToFirst()) {
            do {
                nom = c.getString(c.getColumnIndex(CLAU_NOM));
                // afegeix a la llista d'alumnes
                arrayList_Aliment.add(nom);
            } while (c.moveToNext());
            Log.d("array", arrayList_Aliment.toString());
        }
        return arrayList_Aliment;
    }
    public String obte_kal(String nom) {
        String kal="";
        String selectQuery = "SELECT * FROM " + TAULA_ALIMENTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        return kal;
    }

}
