package cat.dam.lluc.calories;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Main2Activity extends Activity {
    private Button btn_enrera, btn_actualitzar, btn_preparada;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        btn_enrera = (Button)findViewById(R.id.btn_enrera);

        btn_enrera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Main2Activity.this, MainActivity.class);
                Bundle b = new Bundle();
                intent.putExtras(b);
                startActivity(intent);

            }
        });
    }
}

